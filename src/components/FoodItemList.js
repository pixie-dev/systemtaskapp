// @flow

import React, {useContext, useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import OrderFoodContext from '../context/orderFoodContext';

const FoodItemList = ({item, isCart}) => {
  const {name, ingredients, cost, contains, id} = item;
  const context = useContext(OrderFoodContext);
  const [itemCount, setItemCount] = useState(0);
  useEffect(() => {
    const cart = context.cart;
    const currentItem = cart && cart.find((data) => data.id === id);
    if (currentItem) {
      setItemCount(currentItem.quantity);
    } else {
      setItemCount(0);
    }
  }, [context.cart, id]);

  const styles = getStyles(isCart);

  return (
    <React.Fragment>
      <View style={styles.wrapper}>
        {itemCount === 0 && isCart ? null : (
          <OrderFoodContext.Consumer>
            {(conApi) => (
              <View style={styles.container}>
                <View style={styles.containsInfo}>
                  {contains.map((v) => (
                    <Text style={styles.vareityOption}>{v}</Text>
                  ))}
                </View>
                <View style={styles.dishInfo}>
                  <Text style={styles.name}>{name}</Text>
                  <View style={styles.ingredientsWrapper}>
                    {ingredients.map((v) => (
                      <Text style={styles.ingredient}>{v}</Text>
                    ))}
                  </View>
                  <Text style={styles.cost}>€{cost}</Text>
                </View>
                <View style={styles.itemsCountWrapper}>
                  {itemCount === 0 ? (
                    <View style={styles.countButton}>
                      <TouchableOpacity
                        style={styles.addButtonText}
                        onPress={() => conApi.addItemToCart(item)}>
                        <Text style={styles.addText}>Add</Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View style={styles.countButton}>
                      <TouchableOpacity
                        style={styles.removeButton}
                        onPress={() => conApi.removeItemFromCart(id)}>
                        <Text style={styles.addOrRemoveText}>-</Text>
                      </TouchableOpacity>
                      <Text style={styles.quantity}>{itemCount}</Text>
                      {itemCount < 20 ? (
                        <TouchableOpacity
                          style={styles.addButton}
                          onPress={() => conApi.addItemToCart(item)}>
                          <Text style={styles.addOrRemoveText}>+</Text>
                        </TouchableOpacity>
                      ) : null}
                    </View>
                  )}
                  <React.Fragment>
                    {isCart ? (
                      <Icon name="comment-multiple" size={30} color="#555656" />
                    ) : null}
                  </React.Fragment>
                </View>
                <View style={styles.seperator} />
              </View>
            )}
          </OrderFoodContext.Consumer>
        )}
      </View>
    </React.Fragment>
  );
};

const getStyles = (isCart) =>
  StyleSheet.create({
    wrapper: {
      marginRight: -16,
      borderBottomColor: '#afafaf',
      borderBottomWidth: isCart ? 0.6 : 0,
    },
    container: {
      flex: 1,
      flexDirection: 'row',
      paddingTop: 16,
      paddingBottom: 8,
      marginRight: 16,
    },
    containsInfo: {
      paddingTop: 4,
      flex: 0.7,
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
    vareityOption: {
      fontSize: 12,
      borderWidth: 1,
      borderColor: '#afafaf',
      padding: 4,
      marginVertical: 2,
      borderRadius: 3,
    },
    dishInfo: {
      flex: 6.8,
      flexDirection: 'column',
    },
    name: {
      fontSize: 18,
      paddingBottom: 8,
      color: '#555656',
      fontWeight: '400',
    },
    ingredientsWrapper: {
      flexDirection: 'row',
      paddingBottom: 8,
    },
    ingredient: {
      fontSize: 14,
      color: '#555656',
      fontWeight: '300',
    },
    cost: {
      fontSize: 20,
      color: '#ebb97b',
    },
    itemsCountWrapper: {
      paddingTop: 4,
      flex: 2.5,
      flexDirection: 'column',
      alignItems: 'flex-end',
      justifyContent: 'space-between',
      paddingLeft: 16,
    },
    countButton: {
      width: '100%',
      flexDirection: 'row',
      paddingHorizontal: 8,
      paddingVertical: 2,
      borderColor: '#ebb97b',
      borderWidth: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    addOrRemoveText: {
      fontSize: 20,
      fontWeight: '300',
    },
    quantity: {
      fontSize: 14,
    },
    addButtonText: {
      flex: 1,
    },
    addText: {
      fontSize: 14,
      textAlign: 'center',
      padding: 4,
    },
  });

export default FoodItemList;
